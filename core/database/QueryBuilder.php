<?php

class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function selectAll($table)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM {$table}");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function insert($table, $parameters)
    {
        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );

        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function delete($table, $parameters)
    {
        $sql = sprintf(
            "DELETE FROM %s WHERE %s=%s",
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );

        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function update($table,$parameters, $id)
    {
        $fields = [];
        $whereId = [];
        foreach ($parameters as $key => $value)
		{
            $fields[] = $key . ' = ' . "'$value'";
            continue;
        }
        foreach ($id as $key => $value)
		{
            $whereId[] = $key . ' = ' . "'$value'";
            continue;
        }    
        $sql = "UPDATE " . $table . " SET " . implode(', ', $fields) . " WHERE ". implode('&& ', $whereId);

        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        }catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
