<?php
try {
    $dsn = 'mysql:host=localhost;dbname=test;port=3305;charset=utf8';
    $pdo = new PDO($dsn, 'root', '');

    $stmt = $pdo->prepare("UPDATE user SET name=:name,age=:age WHERE id=:id");

    $stmt->bindParam(':name', $name, PDO::PARAM_STR);
    $stmt->bindParam(':age', $age, PDO::PARAM_INT);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);

    $name = '小花';
    $age = 16;
    $id = 1;

    $stmt->execute();

    if ($stmt->rowCount()) {
        echo '修改成功';
    } else {
        echo '修改失敗';
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
