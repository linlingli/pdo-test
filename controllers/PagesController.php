<?php

class PagesController
{
    public function index()
    {
        $userList = App::get('database')->selectAll('user');
        require 'views/index.view.php';
    }

    public function insert()
    {
        App::get('database')->insert('user', [
            'name' => $_POST['name'],
            'age' => $_POST['age'],
            'sex' => $_POST['sex'],
        ]);

        header('Location : /');
    }

    public function delete()
    {
        App::get('database')->delete('user', [
            'id' => $_GET['id'],
        ]);

        header('Location : /');
    }

    public function update()
    {
        App::get('database')->update('user', [
            'name' => $_POST['name'],
            'age' => $_POST['age'],
            'sex' => $_POST['sex'],
        ], ['id' => $_POST['id']]);

        header('Location : /');
    }
}
