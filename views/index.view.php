<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <p>Add</p>
    <form method="POST" action="/insert">
        name<input name="name" type="text">
        age<input name="age" type="text">
        sex<input name="sex" type="text">
        <button type="submit">submit</button>
    </form>
    <br>

    <p>Updata</p>
    <form method="POST" action="/update">
        id<input name="id" type="text">
        name<input name="name" type="text">
        age<input name="age" type="text">
        sex<input name="sex" type="text">
        <button type="submit">submit</button>
    </form>
    <br>

    <p>Delete</p>
    <form method="GET" action="/delete">
        id<input name="id" type="text">
        <button type="submit">submit</button>
    </form>
    <br>

    <p>List</p>
    <table>
        <?php foreach ($userList as $value) : ?>
            <tr>
                <td><?= json_encode($value); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>

</html>