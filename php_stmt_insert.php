<?php
try {
    $dsn = 'mysql:host=localhost;dbname=test;port=3305;charset=utf8';
    $pdo = new PDO($dsn, 'root', '');

    $stmt = $pdo->prepare("INSERT INTO user(name,sex,age) VALUES(?,?,?)");

    $stmt->bindParam(1, $name, PDO::PARAM_STR);
    $stmt->bindParam(2, $sex, PDO::PARAM_INT);
    $stmt->bindParam(3, $age, PDO::PARAM_INT);

    $name = '小豬';
    $sex = 2;
    $age = 18;

    $result = $stmt->execute();

    if ($stmt->rowCount()) {
        echo '新增成功';
    } else {
        echo '新增失敗';
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
