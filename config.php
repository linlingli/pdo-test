<?php

return [
    'database' => [
        'name' => 'test',
        'port' => '3305',
        'username' => 'root',
        'password' => '',
        'connection' => 'mysql:host=localhost',
        'options' => [
            PDO::ATTR_ERRMODE =>PDO::ERRMODE_EXCEPTION
        ]
    ]
];
